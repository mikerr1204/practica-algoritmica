function solution(N, A) {
    let counters = new Array(N).fill(0);
    
    A.forEach(element => {
        (element>=1 && element<=N)?
        counters[element-1]++:counters.fill(Math.max(...counters));
    });
    return counters;
}
function solutionValidando(N, A) {
    if (!Number.isInteger(N) || N < 1 || N > 100000 || A.length < 1 || A.length > 100000) {
        throw new Error('N y M deben ser números enteros en el rango [1..100,000]');
    }
    let counters = new Array(N).fill(0);
    A.forEach(element => {
        if(element>=1 && element<=N){
            counters[element-1]++
        }else if(element==N+1){
            counters.fill(Math.max(...counters))
        }else{
            throw new Error('Cada elemento de A debe ser un número entero en el rango [1..N + 1]');
        }
    });
    return counters;
}
console.log(solution(5, [1, 1, 4, 6, 1, 5, 5])); // debería imprimir [3, 2, 2, 4, 2]
console.log(solutionValidando(5, [1, 1, 4, 6, 1, 5, 5])); // debería imprimir [3, 2, 2, 4, 2]

